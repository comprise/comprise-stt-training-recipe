.PHONY: docker push

IMAGE            ?= comprisedev.azurecr.io/comprise-stt-job
VERSION          ?= $(shell git describe --tags --always --dirty)
TAG              ?= latest

default: docker

docker: 
	docker build --build-arg "VERSION=$(VERSION)" -t "$(IMAGE):$(TAG)" .
	@echo 'Docker image $(IMAGE):$(TAG) can now be used.'

push: docker
	docker push "$(IMAGE):$(TAG)"
