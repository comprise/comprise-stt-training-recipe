# create image from comprise-job-wrapper
FROM comprisedev.azurecr.io/comprise-kaldi-job-wrapper:e6aab67

# install kaldi-lm
# install OpenGRM
# install espeak
# install err2unk dependencies
# install rhasspy-nlu
RUN cd /opt/kaldi/tools && extras/install_kaldi_lm.sh && \
    extras/install_opengrm.sh && \
    apt-get update && apt-get install -y espeak bc python3.7-dev curl python3.7-venv && \
    python3.7 -m pip install tensorflow==2.0.0 keras==2.3.1 sklearn && \
    python3.7 -m pip install https://github.com/kpu/kenlm/archive/master.zip && \
    pip3 install 'h5py==2.10.0' --force-reinstall && \
    cd /opt/kaldi/src/latbin && \
    wget https://gitlab.inria.fr/comprise/speech-to-text-weakly-supervised-learning/-/raw/master/local/kaldi-helpers/lattice-mbr-decode-wtimes.cc?inline=false \
    -O lattice-mbr-decode-wtimes.cc && \
    sed -i "s;lattice-reverse;lattice-reverse lattice-mbr-decode-wtimes;" Makefile && make lattice-mbr-decode-wtimes && \
    python3.7 -m pip install rhasspy-nlu
#    cd /opt/ && git clone https://github.com/rhasspy/rhasspy-nlu && cd rhasspy-nlu && ./configure && make && PYTHON=python3.7 make install && python3.7 setup.py sdist

# copy entry shell script
COPY job.sh /
COPY copy_job.sh /

# copy component entry script
COPY component.sh /

# copy component configs
COPY component-lm.yaml /
COPY component-weaksup.yaml /
COPY component-gram.yaml /
COPY copy-deps.yaml /

# copy training scripts
COPY path.sh /recipe/
COPY cmd.sh /recipe/
COPY local /recipe/local
COPY conf /recipe/conf

RUN ln -s /opt/kaldi/egs/wsj/s5/steps /recipe/steps
RUN ln -s /opt/kaldi/egs/wsj/s5/utils /recipe/utils

# set routing key to accept only specific training requests
CMD ["-r ASR.base"]
