#!/bin/sh

mv /copy-deps.yaml /component-main.yaml

mv /recipe/local/copy_model.sh /job.sh

cd /api-server
exec /tini -- python3.7 src/job_wrapper.py "$@"
