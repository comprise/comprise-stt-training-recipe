#!/bin/sh
lang=$1

cd /recipe

local/prepare_data.sh $lang
local/train_lm.sh /data/kaldi-data-transcribed/
local/format_local_lm.sh --datadir /data/kaldi-data-transcribed
local/train_tri3.sh
local/train_chain.sh $lang

# validation
local/validate_model.sh

# Err2Unk training is asked by setting environment variable in Kubernetes job definition
if [ ${ERR2UNK} ]; then
  local/run_err2unk.sh $lang
else

  # logs
  tar -czf /logs.tgz exp/chain/tdnn/log

  # prepare trained model for export
  mkdir -p /model/tdnn
  for i in frame_subsampling_factor srand cmvn_opts lda.mat tree configs final.ie.id lda_stats accuracy.report final.mdl phones.txt lm_wt ; do
    cp -r exp/chain/tdnn/$i /model/tdnn/
  done

  # move decoding graphs
  for i in graph graph_lookahead graph_lookahead_arpa ; do
    mv exp/chain/tdnn/$i /model/$i
  done

  mv exp/chain/tdnn_online /model/
  sed -i "s;/recipe/exp/chain/;;" /model/tdnn_online/conf/*.conf
  tar -czf /model.mdl /model

  # package some components separately
  mkdir -p /components
  tar -czf /components/graph_lookahead /model/graph_lookahead
  tar -czf /components/graph_lookahead_arpa /model/graph_lookahead_arpa
  tar -czf /components/tdnn /model/tdnn
  tar -czf /components/tdnn_online /model/tdnn_online
  mv /data/kaldi-data-transcribed/3gram/lm_unpruned.gz /components/lm_arpa

fi
