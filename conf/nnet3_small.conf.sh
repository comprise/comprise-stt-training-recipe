#!/usr/bin/env bash

set -e

# training options
export xent_regularize=0.1
export dropout_schedule='0,0@0.20,0.1@0.50,0'
export leaky_hmm_coefficient=0.1
export l2_regularize=0.00005 
export num_chunk_per_minibatch=128,256,64
export frames_per_iter=3000000
export num_epochs=4
export num_jobs_initial=1
export num_jobs_final=1
export initial_effective_lrate=0.001
export final_effective_lrate=0.0001
export max_param_change=2.0
export shrink_value=1.0
export proportional_shrink=60.0
export momentum=0.0 
export chunk_width=140,100,160 
export chunk_left_context=0
export chunk_right_context=0
export chunk_left_context_initial=0
export chunk_right_context_final=0
export egs_constrained=true

speed_perturb=true
dir=exp/chain/tdnn

. ./cmd.sh
. ./path.sh
. ./utils/parse_options.sh

suffix=
$speed_perturb && suffix=_sp

treedir=exp/chain/tri5_7d_tree$suffix

echo "$0: creating neural net configs using the xconfig parser";

num_targets=$(tree-info $treedir/tree |grep num-pdfs|awk '{print $2}')
learning_rate_factor=$(echo "print (0.5/$xent_regularize)" | python)

mkdir -p $dir/configs

cat <<EOF > $dir/configs/network.xconfig
 input dim=100 name=ivector
 input dim=40 name=input

 # please note that it is important to have input layer with the name=input
 # as the layer immediately preceding the fixed-affine-layer to enable
 # the use of short notation for the descriptor
 fixed-affine-layer name=lda input=Append(-2,-1,0,1,2,ReplaceIndex(ivector, t, 0)) affine-transform-file=$dir/configs/lda.mat

 # the first splicing is moved before the lda layer, so no splicing here
 relu-renorm-layer name=tdnn1 dim=512
 relu-renorm-layer name=tdnn2 dim=512 input=Append(-1,0,1)
 relu-renorm-layer name=tdnn3 dim=512 input=Append(-1,0,1)
 relu-renorm-layer name=tdnn4 dim=512 input=Append(-3,0,3)
 relu-renorm-layer name=tdnn5 dim=512 input=Append(-3,0,3)
 relu-renorm-layer name=tdnn6 dim=512 input=Append(-6,-3,0)

 ## adding the layers for chain branch
 relu-renorm-layer name=prefinal-chain dim=512 target-rms=0.5
 output-layer name=output include-log-softmax=false dim=$num_targets max-change=1.5

 # adding the layers for xent branch
 # This block prints the configs for a separate output that will be
 # trained with a cross-entropy objective in the 'chain' models... this
 # has the effect of regularizing the hidden parts of the model.  we use
 # 0.5 / args.xent_regularize as the learning rate factor- the factor of
 # 0.5 / args.xent_regularize is suitable as it means the xent
 # final-layer learns at a rate independent of the regularization
 # constant; and the 0.5 was tuned so as to make the relative progress
 # similar in the xent and regular final layers.
 relu-renorm-layer name=prefinal-xent input=tdnn6 dim=512 target-rms=0.5
 output-layer name=output-xent dim=$num_targets learning-rate-factor=$learning_rate_factor max-change=1.5

EOF

steps/nnet3/xconfig_to_configs.py --xconfig-file $dir/configs/network.xconfig --config-dir $dir/configs/

