#!/bin/sh
lang=$1
component=$2

cd /recipe

. ./path.sh

if [ "$component" = "lm_arpa" ]; then
  local/prepare_data.sh $lang
  local/train_lm.sh /data/kaldi-data-transcribed/
  local/format_local_lm.sh --datadir /data/kaldi-data-transcribed
  mkdir -p /components
  mv /data/kaldi-data-transcribed/3gram/lm_unpruned.gz /components/lm_arpa
fi

if [ "$component" = "lm" ]; then
  # pre-process text data to monolingual corpus
  python3.7 /api-server/utils/text_data_prep.py  
  lm_train_set=/data/text-data-raw

  # remove non-printable characters
  LC_ALL="C.UTF-8" sed -i "s;[^[:print:][:space:]];;g" ${lm_train_set}/text

  # unpack acoustic model  
  tar -xzf /data/deps/am.tgz 
  dir=model/tdnn

  # preparel lexicon
  if [ $lang = "lt" ]; then
      local/prepare_dict_lt.sh --locdata ${lm_train_set}
  else
      local/prepare_dict_espeak.sh --lang $lang --locdata ${lm_train_set}
  fi

  LC_ALL=C utils/prepare_lang.sh data/dict "<unk>" data/lang_tmp data/lang || exit 1;

  # train LM
  local/train_lm.sh ${lm_train_set}

  # create G.fst
  local/format_local_lm.sh --datadir ${lm_train_set}

  # create HCLG graph, as well as Gr and HCLr graphs with lookahead composition
  utils/mkgraph_lookahead.sh --self-loop-scale 1.0 --remove-oov --compose-graph ${lm_train_set}/lang_test_tg $dir $dir/graph_lookahead

  # package component
  mkdir -p /components
  mkdir -p /model
  mv $dir/graph_lookahead /model/graph_lookahead
  tar -czf /components/lm /model/graph_lookahead
  mv ${lm_train_set}/3gram/lm_unpruned.gz /components/lm_arpa
fi

if [ "$component" = "weaksup" ]; then
  echo "unpacking dependencies"
  tar -xzf /data/deps/am.tgz 
  mkdir -p exp/chain
  mv model/tdnn exp/chain

  mkdir -p /data/kaldi-data-transcribed/3gram/
  mv /data/deps/lm_arpa /data/kaldi-data-transcribed/3gram/lm_unpruned.gz
 
  tar -xzf /data/deps/tdnn_online
  mkdir -p exp/nnet3
  mv model/tdnn_online exp/chain
  cp -r exp/chain/tdnn_online/ivector_extractor exp/nnet3/extractor

  tar -xzf /data/deps/graph_lookahead
  mv model/graph_lookahead exp/chain/tdnn/graph

  echo "preparing data"
  local/prepare_data.sh $lang
  rm data/dev/feats.scp # delete low-res feats.scp

  echo "starting err2unk"
  local/run_err2unk.sh $lang
fi

if [ "$component" = "gram" ]; then
  # unpack acoustic model  
  tar -xzf /data/deps/am.tgz 
  dir=model/tdnn

  # background arpa, grammar file and mixing coefficient
  bg_arpa=/data/deps/background.arpa
  gunzip $bg_arpa.gz
  gram_file=/data/attachement
  if [ ! -f $gram_file ]; then
    echo "Exitting. No grammar file provided!"
    exit 1;
  fi
  tr -d '\r' < $gram_file > $gram_file.2
  mv $gram_file.2 $gram_file # convert line endings

  new_arpa_dir=/data/gram_arpa_mix
  mkdir -p ${new_arpa_dir}

  local/gram/mix_grammar_n_arpa.sh ${gram_file} ${bg_arpa} ${new_arpa_dir}
  grep -P "^\-\d\.\d" ${new_arpa_dir}/gram_bg-arpa_merge.arpa | cut -f1-2 | tr "\t" " " | grep -Pv "\<\/?s\>" > ${new_arpa_dir}/trig_probs 
  cp ${new_arpa_dir}/trig_probs ${new_arpa_dir}/text # since we don't have a lm_train_set text file we use this trick to resuse local/prepare_dict_*.sh in following steps

  # preparel lexicon
  if [ $lang = "lt" ]; then
      local/prepare_dict_lt.sh --locdata ${new_arpa_dir}
  else
      local/prepare_dict_espeak.sh --lang $lang --locdata ${new_arpa_dir}
  fi

  LC_ALL=C utils/prepare_lang.sh data/dict "<unk>" data/lang_tmp data/lang || exit 1;

  # prepare for local/format_local_lm.sh and create G.fst
  mkdir -p ${new_arpa_dir}/lang_test_tg
  mkdir -p ${new_arpa_dir}/3gram
  gzip < ${new_arpa_dir}/gram_bg-arpa_merge.arpa > ${new_arpa_dir}/3gram/lm_unpruned.gz
  local/format_local_lm.sh --datadir ${new_arpa_dir}

  # create HCLG graph, as well as Gr and HCLr graphs with lookahead composition
  utils/mkgraph_lookahead.sh --self-loop-scale 1.0 --remove-oov --compose-graph ${new_arpa_dir}/lang_test_tg $dir $dir/graph_lookahead

  # package component
  mkdir -p /components
  mkdir -p /model
  mv $dir/graph_lookahead /model/graph_lookahead
  tar -czf /components/gram /model/graph_lookahead
fi
