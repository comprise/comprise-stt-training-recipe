A speech recognition grammar contains a set of rules which construct different possible queries that can be recognised by a Speech-To-Text (STT) system. [JSGF](https://www.w3.org/TR/jsgf/) and [SRGS](https://www.w3.org/TR/speech-grammar/) are examples of standard speech recognition grammar formats.

This document describes the format of the speech recognition grammar supported by the COMPRISE cloud platform.

----
## Table of Contents
* [Grammar Format](#grammar-format)
	- [Basic Constructs](#basic-constructs)
	- [Grouping and Reuse](#grouping-and-reuse)
* [Fallback Language Model](#fallback-language-model)
* [Header](#header)
* [Comments](#comments)
* [Examples](#examples)
  - [Cooking App Grammar](#cooking-app-grammar) 
  - [Shopping App Grammar](#shopping-app-grammar)
----

## Grammar Format
A grammar can be composed of a simple set of sentences or a set of constructs that follow a particular syntax and generate multiple sentences. The format for listing simple sentences and more complex constructs are detailed below.

### Basic Constructs
- Simple Sentences

	Simple sentences can be listed in the grammar file, one per line, as follows.

	```
	order a pizza for two
	buy two coupons for dinner
	reserve a table for four
	```

- Optional Words

	Words which are optional in a sentence can be marked within square brackets `[]`. For example:

	```
	order [a] pizza for two
	buy [two] coupons for dinner
	reserve a table [for four]
	```

- Alternatives

	Different alternatives for a particular word can be listed with the `|` symbol within parenthesis `()`. For example:

	```
	order a pizza for (two | three | four) people
	buy (two | three) coupons for (lunch | dinner)
	```

### Grouping and Reuse
- Sections

	A grammar file groups sentences or grammar constructs in seperate sections as follows.

	```
	[Reserve]
	reserve a table [for four people]
	buy (two | three) coupons for (lunch | dinner)

	[Order]
	order [a] pizza for (two | three | four) [people]
	order a (burger | sandwich) [menu]
	```

	The name of the sections, `[Reserve]` and `[Order]` in examples above, are simply placeholders that improve readability and not used in the STT.

- Named Constructs

	The sentences and constructs discussed above can be defined as a named construct within a section. Creating a named construct helps to simplify complex contructs and to reuse them within the same section as well as in another section. 
	
	A named construct is defined using the `=` sign and referred using the `<>` brackets. For example:

	```
	[Reserve]
	place = (table | room)
	guests = (two | three | four) [people]
	reserve [a] <place> for <guests>

	[Order]
	order [a] pizza for <Reserve.guests>
	```

## Fallback language model
The sentences and constructs listed in the grammar file will result into a closed set of queries. STT based only on such a closed grammar  will fail to recognise words and queries which are not part of the grammar. To handle this, the grammar is mixed with a generic language model (LM). The [header](#header) of the grammar file specifies the weightage given to the fallback LM. 

It must be noted that the fallback LM itself is a closed vocabulary model and it may not be able to recognise very specific words likes proper names, product names, acronyms, etc.

## Header
The first line of the grammar file is expected to be a header defining the weightage assigned to the [fallback LM](#fallback-language-model). The header line begins with the `#` symbol and specifices the weight as follows:

```
#mix_coeff=0.1
```

This implies that the fallback LM has a weightage of 0.1 and the grammar has a weightage of 0.9.

## Comments
The grammar file can contain comment lines, except at the first line which is reserved for [header](#header). Every comment line should begin with the `#` symbol, followed by the comment. For example:

```
[BookRoom]
rooms = (one room | two rooms | three rooms)
guests = (one person | two persons | family of three | famiy of four)
book <rooms> for <guests>

#Note: allows only 1/2/3 rooms for 1/2/3/4 guests
```

## Examples

### Cooking App Grammar
```
#mix_coeff=0.5

[SmallTalk]
(hi | hey | hello) [bot]
(thank you | thanks)

[Recipe]
dessert = (strawberry | chocolate | vanilla) (cake | mousse | ice cream)

[AskRecipe]
make = (do | make | prepare)
i would like to <make> [a] <Recipe.dessert> 
how to <make> [a] <Recipe.dessert> 
(show me | read) [a | the] recipe for <Recipe.dessert>
```

### Shopping App Grammar
```
#mix_coeff=0.7

[StapleItems]
number = (two | three | four | five)
item = <number> packs of (pasta | flour | rice)

[FruitsVeggies]
fruits = (apples | bananas | grapes)
veggies = (onions | carrots | potatoes)
amount = (half | one | two) kilograms
item = <amount> of (<fruits> | <veggies>)

[RequestItems]
item = (<FruitsVeggies.item> | <StapleItems.item>)
add <item> [to the cart]
remove <item> [from the cart]
```
