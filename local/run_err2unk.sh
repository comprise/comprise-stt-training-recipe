#!/bin/bash
lang=$1

cd /recipe

. ./path.sh

local/prepare_err2unk_semisup.sh

local/train_lm.sh data/train_e2u_semisup

local/format_local_lm.sh --datadir data/train_e2u_semisup

local/train_tri3.sh --src_datadir data/train_e2u_semisup \
		--expdir exp/tri3_e2u_semisup \
		--trainSplit train_e2u_semisup 

local/train_chain.sh --nnet3_affix e2u \
		--dir exp/chain/tdnn_e2u \
		--train_set data/train_e2u_semisup \
		--lm_train_set data/train_e2u_semisup \
		--ali_dir exp/tri4_e2u_ali \
		--lang data/lang_chain_e2u_2y \
		--tri4 exp/tri3_e2u_semisup/tri3_full \
		$lang

# validation and logs
local/validate_model.sh --am exp/chain/tdnn_e2u --nnet3_affix e2u --suffix e2u --stage 0
tar -czf /logs.tgz exp/chain/tdnn_e2u/log

# prepare trained model for export
rm -rf /model
mkdir -p /model/tdnn
for i in frame_subsampling_factor srand cmvn_opts lda.mat tree configs final.ie.id lda_stats accuracy.report final.mdl phones.txt lm_wt ; do
  cp -r exp/chain/tdnn_e2u/$i /model/tdnn/
done

# filter and move decoding graphs
for i in graph graph_lookahead graph_lookahead_arpa ; do
  # get <unk> id
  unkid=`grep "<unk>" exp/chain/tdnn_e2u/$i/words.txt | cut -f2 -d" "`
  fstrmsymbols --remove-arcs=true --apply-to-output=true <(echo $unkid) exp/chain/tdnn_e2u/$i/{HCLG.fst,HCLG2.fst}
  mv exp/chain/tdnn_e2u/$i/{HCLG2.fst,HCLG.fst}
  mv exp/chain/tdnn_e2u/$i /model/$i
done

mv exp/chain/tdnn_e2u_online /model/tdnn_online
sed -i "s;/recipe/exp/chain/;;" /model/tdnn_online/conf/*.conf
tar -czf /model.mdl /model

# package some components separately
mkdir -p /components
tar -czf /components/graph_lookahead /model/graph_lookahead
tar -czf /components/tdnn /model/tdnn
tar -czf /components/tdnn_online /model/tdnn_online
mv data/train_e2u_semisup/3gram/lm_unpruned.gz /components/lm_arpa
