#!/bin/bash

# Train GMM-HMM models for alignments
#

src_datadir=data/train # absolute path to source data dir

datadir=data/
expdir=exp/tri3-seed/
confdir=conf/
localdir=local/

split_stage=true    # set to false if mono/tri1/tri2 training spliting step is already completed.
mono_stage=true     # set to false if mono training step is already completed.
tri1_stage=true     # set to false if tri1 training step is already completed.
tri2_stage=true     # set to false if tri2 training step is already completed.
tri3_stage=true      # tri3 on partial trainset; set to false if this training step is already completed.
tri3_full_stage=true # tri3 on full trainset; set to false if this training step is already completed.

feats_nj=4	     # num of jobs for feat extraction
train_nj=4	     # num of jobs for train

# Acoustic model parameters (Note: u may have to vary this based on size of your datasets !!!)
numLeavesTri1=2000
numGaussTri1=10000
numLeavesTri2=2500
numGaussTri2=15000
numLeavesTri3=2500
numGaussTri3=15000
numLeavesTri3Full=3500
numGaussTri3Full=25000

# scale parameters up if dataset is bigger 
data_size=`cut -f2 -d" " $src_datadir/utt2dur | paste -sd+ | bc | cut -f1 -d"."`
data_size=`echo $data_size/3600 | bc`

if [ $data_size -gt 80 ]; then
  numLeavesTri3Full=4000
  numGaussTri3Full=90000
fi

if [ $data_size -gt 190 ]; then
  numLeavesTri3Full=12000
  numGaussTri3Full=220000
fi

# number of utterances for different AM (Note: u may have to vary this based on size of your datasets !!!)
monoSize=1   # shortest, about 1hr
tri1Size=10   # random, about 10hr
tri2Size=50   # random, about 50hr
tri3Size=200   # random, about 200hr 

. ./path.sh  # set paths to binaries
. ./cmd.sh   # This relates to the execution queue.

trainSplit=train

set -e

. utils/parse_options.sh  # e.g. this parses the options if supplied. (currently disabled.)

if $split_stage; then
  echo ============================================================================
  echo "                Create Mono/Tri1/Tri2 splits for Training                 "
  echo ============================================================================
  ## make subset with shortest $monoSize utt (?hr data) to train mono models
  local/data/subset_mono_data.sh $src_datadir $monoSize $datadir/split/$trainSplit"_s"$monoSize

  ## make subset with random $tri1Size utterances to train tri1 models.
  local/data/subset_data_dir.sh $src_datadir $tri1Size $datadir/split/$trainSplit"_r"$tri1Size || exit 1; 

  ## make subset with random $tri2Size utterances to train tri2 models.
  local/data/subset_data_dir.sh $src_datadir $tri2Size $datadir/split/$trainSplit"_r"$tri2Size || exit 1;  

  ## make subset with random $tri3Size utterances to train tri3 models.
  local/data/subset_data_dir.sh $src_datadir $tri3Size $datadir/split/$trainSplit"_r"$tri3Size || exit 1;  
fi

if $mono_stage; then
  echo ============================================================================
  echo "                     MonoPhone Training                                   "
  echo ============================================================================
  steps/train_mono.sh --nj "$train_nj" --cmd "$train_cmd" $datadir/split/$trainSplit"_s"$monoSize $datadir/lang $expdir/mono

fi

if $tri1_stage; then
  echo ============================================================================
  echo "           tri1 : Deltas + Delta-Deltas Training                          "
  echo ============================================================================
  # Align te-in-Train_r5k data with mono models.
  steps/align_si.sh --boost-silence 1.25 --nj "$train_nj" --cmd "$train_cmd" $datadir/split/$trainSplit"_r"$tri1Size $datadir/lang $expdir/mono $expdir/mono_ali

  # From monophone model, train tri1 which is Deltas + Delta-Deltas.
  steps/train_deltas.sh --cmd "$train_cmd" $numLeavesTri1 $numGaussTri1 $datadir/split/$trainSplit"_r"$tri1Size $datadir/lang $expdir/mono_ali $expdir/tri1

fi 

if $tri2_stage; then
  echo ============================================================================
  echo "                 tri2 : LDA + MLLT Training                               "
  echo ============================================================================
  # Align te-in-Train_r10k data with tri1 models.
  steps/align_si.sh --nj "$train_nj" --cmd "$train_cmd" $datadir/split/$trainSplit"_r"$tri2Size $datadir/lang $expdir/tri1 $expdir/tri1_ali

  # From tri1 system, train tri2 which is LDA + MLLT.
  steps/train_lda_mllt.sh --cmd "$train_cmd" --splice-opts "--left-context=3 --right-context=3" $numLeavesTri2 $numGaussTri2 $datadir/split/$trainSplit"_r"$tri2Size $datadir/lang $expdir/tri1_ali $expdir/tri2

fi

if $tri3_stage; then
  echo ============================================================================
  echo "              tri3 : LDA + MLLT + SAT Training                            "
  echo ============================================================================
  # Align te-in-Train_r10k data with tri2 models.
  steps/align_si.sh --nj "$train_nj" --cmd "$train_cmd" --use-graphs true $datadir/split/$trainSplit"_r"$tri2Size $datadir/lang $expdir/tri2 $expdir/tri2_ali

  # From tri2 system, train tri3 which is LDA + MLLT + SAT.
  steps/train_sat.sh --cmd "$train_cmd" $numLeavesTri3 $numGaussTri3 $datadir/split/$trainSplit"_r"$tri2Size $datadir/lang $expdir/tri2_ali $expdir/tri3

fi

if $tri3_full_stage; then
  echo ============================================================================
  echo "              tri3 full: LDA + MLLT + SAT Training                        "
  echo ============================================================================
  # Align full train data with tri3 models.
  steps/align_si.sh --nj "$train_nj" --cmd "$train_cmd" --use-graphs true $src_datadir $datadir/lang $expdir/tri3 $expdir/tri3_ali

  # From tri3 system, train tri3_full which is also LDA + MLLT + SAT.
  steps/train_sat.sh --cmd "$train_cmd" $numLeavesTri3Full $numGaussTri3Full $datadir/split/$trainSplit"_r"$tri3Size $datadir/lang $expdir/tri3_ali $expdir/tri3_full

fi

