#!/usr/bin/env bash

# copied from swbd 7q recipe

set -e

# configs for 'chain'
stage=0
train_stage=-10
get_egs_stage=-10
speed_perturb=true

# training options
remove_egs=true
common_egs_dir=

nnet3_affix=
suffix=
dir=exp/chain/tdnn

train_set=data/train
lm_train_set=/data/kaldi-data-transcribed
ali_dir=exp/tri4_ali
lang_ali=data/lang
lang=data/lang_chain_2y
tri4=exp/tri3-seed/tri3_full

# End configuration section.
echo "$0 $@"  # Print the command line for logging

. ./cmd.sh
. ./path.sh
. ./utils/parse_options.sh

$speed_perturb && suffix=_sp
asr_lang=$1
treedir=exp/chain/tri5_7d_tree${suffix}

if [ -z "$nnet3_affix" ]; then
  nnet3_affix_opt=
else
  nnet3_affix_opt="--nnet3_affix $nnet3_affix"
fi



if ! cuda-compiled; then
  cat <<EOF && exit 1
This script is intended to be used with GPUs but you have not compiled Kaldi with CUDA
If you want to use GPUs (and have them), go to src/, and configure and make on a machine
where "nvcc" is installed.
EOF
fi

# if we are using the speed-perturbed data we need to generate
# alignments for it.
local/nnet3/run_ivector_common.sh --stage $stage \
  --speed-perturb $speed_perturb \
  --generate-alignments $speed_perturb \
  --gmm $tri4 \
  --ali_dir $ali_dir \
  $nnet3_affix_opt \
  $train_set || exit 1;


if [ $stage -le 9 ]; then
  # Get the alignments as lattices (gives the LF-MMI training more freedom).
  # use the same num-jobs as the alignments
  nj=$(cat $ali_dir/num_jobs) || exit 1;
  steps/align_fmllr_lats.sh --nj $nj --cmd "$train_cmd" ${train_set}${suffix} \
    $lang_ali $tri4 exp/${tri4}_lats${suffix}
  rm exp/${tri4}_lats${suffix}/fsts.*.gz # save space
fi


if [ $stage -le 10 ]; then
  # Create a version of the lang/ directory that has one state per phone in the
  # topo file. [note, it really has two states.. the first one is only repeated
  # once, the second one has zero or more repeats.]
  rm -rf $lang
  cp -r $lang_ali $lang
  silphonelist=$(cat $lang/phones/silence.csl) || exit 1;
  nonsilphonelist=$(cat $lang/phones/nonsilence.csl) || exit 1;
  # Use our special topology... note that later on may have to tune this
  # topology.
  steps/nnet3/chain/gen_topo.py $nonsilphonelist $silphonelist >$lang/topo
fi

if [ $stage -le 11 ]; then
  # Build a tree using our new topology. This is the critically different
  # step compared with other recipes.
  steps/nnet3/chain/build_tree.sh --frame-subsampling-factor 3 \
      --context-opts "--context-width=2 --central-position=1" \
      --cmd "$train_cmd" 7000 ${train_set}${suffix} $lang $ali_dir $treedir
fi

if [ $stage -le 12 ]; then
  echo "$0: creating neural net configs using the xconfig parser";
  # scale parameters up if dataset is bigger 
  data_size=`cut -f2 -d" " ${train_set}/utt2dur | paste -sd+ | bc | cut -f1 -d"."`
  data_size=`echo $data_size/3600 | bc`

  # TODO: scale configs
  if [ $data_size -le 50 ]; then
    . conf/nnet3_small.conf.sh --speed_perturb $speed_perturb --dir $dir
  fi
  if [ $data_size -gt 50 ]; then
    . conf/nnet3_large.conf.sh --speed_perturb $speed_perturb --dir $dir
  fi
fi

if [ $stage -le 13 ]; then

  steps/nnet3/chain/train.py --stage $train_stage \
    --cmd "$train_cmd" \
    --use-gpu wait \
    --feat.online-ivector-dir exp/nnet3${nnet3_affix}/ivectors \
    --feat.cmvn-opts "--norm-means=false --norm-vars=false" \
    --chain.xent-regularize $xent_regularize \
    --chain.leaky-hmm-coefficient $leaky_hmm_coefficient \
    --chain.l2-regularize $l2_regularize \
    --chain.apply-deriv-weights false \
    --chain.lm-opts="--num-extra-lm-states=2000" \
    --trainer.dropout-schedule $dropout_schedule \
    --trainer.add-option="--optimization.memory-compression-level=2" \
    --egs.dir "$common_egs_dir" \
    --egs.stage $get_egs_stage \
    --egs.opts "--frames-overlap-per-eg 0 --constrained $egs_constrained" \
    --trainer.num-chunk-per-minibatch $num_chunk_per_minibatch \
    --trainer.frames-per-iter $frames_per_iter \
    --trainer.num-epochs $num_epochs \
    --trainer.optimization.num-jobs-initial $num_jobs_initial \
    --trainer.optimization.num-jobs-final $num_jobs_final \
    --trainer.optimization.initial-effective-lrate $initial_effective_lrate \
    --trainer.optimization.final-effective-lrate $final_effective_lrate \
    --trainer.max-param-change $max_param_change \
    --trainer.optimization.shrink-value=$shrink_value \
    --trainer.optimization.proportional-shrink=$proportional_shrink \
    --trainer.optimization.momentum=$momentum \
    --egs.chunk-width=$chunk_width \
    --egs.chunk-left-context=$chunk_left_context \
    --egs.chunk-right-context=$chunk_right_context \
    --egs.chunk-left-context-initial=$chunk_left_context_initial \
    --egs.chunk-right-context-final=$chunk_right_context_final \
    --egs.dir="$common_egs_dir" \
    --cleanup.remove-egs $remove_egs \
    --feat-dir ${train_set}${suffix}_hires \
    --tree-dir $treedir \
    --lat-dir exp/${tri4}_lats${suffix} \
    --dir $dir  || exit 1;

fi

if [ $stage -le 14 ]; then
  # create HCLG graph, as well as Gr and HCLr graphs with lookahead composition
  utils/mkgraph_lookahead.sh --self-loop-scale 1.0 --remove-oov --compose-graph ${lm_train_set}/lang_test_tg $dir $dir/graph_lookahead

  # create HCLG graph, as well as even smaller Gr and HCLr graphs with lookahead composition from arpa
  utils/mkgraph_lookahead.sh --self-loop-scale 1.0 --compose-graph ${lm_train_set}/lang_test_tg $dir ${lm_train_set}/3gram/lm_unpruned.gz $dir/graph_lookahead_arpa

  # create softlink for compatibility 
  ln -s graph_lookahead $dir/graph
fi


if [ $stage -le 16 ]; then
  steps/online/nnet3/prepare_online_decoding.sh \
       --mfcc-config conf/mfcc_hires.conf \
       $lang exp/nnet3${nnet3_affix}/extractor $dir ${dir}_online

  rm $dir/.error 2>/dev/null || true
fi


exit 0;
