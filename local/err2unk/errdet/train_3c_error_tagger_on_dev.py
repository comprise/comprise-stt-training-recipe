#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Copyright © 2020 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

import numpy, sys, os

from keras.models import Sequential, load_model
from keras.layers import Dense, LSTM, Bidirectional, TimeDistributed, Masking
from keras.callbacks import EarlyStopping

from keras.callbacks import Callback
import tensorflow as tf
from keras import backend as K

from sklearn.metrics import classification_report, recall_score, precision_score, f1_score
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split

import pickle

class EarlyStoppingAtMaxF1(tf.keras.callbacks.Callback):
  """Stop training when the F1 is at its max, i.e. the F1 stops increasing.

  Arguments:
      patience: Number of epochs to wait after max has been hit. After this
      number of no improvement, training stops.
  """

  def __init__(self, modelTag, patience=10):
    super(EarlyStoppingAtMaxF1, self).__init__()

    self.patience = patience

    # best_weights to store the weights at which the minimum loss occurs.
    self.best_weights = None

    self.f1_scores=[]

    self.modelTag = modelTag

  def on_train_begin(self, logs=None):
    # The number of epoch it has waited when loss is no longer minimum.
    self.wait = 0
    # The epoch the training stops at.
    self.stopped_epoch = 0
    # Initialize the best as infinity.
    self.best = -1
    self.bestEpoch = -1

  def on_epoch_end(self, epoch, logs=None):    
    y_val_pred=self.model.predict(self.validation_data[0])
    yt = []
    yp = []
    for i in range(self.validation_data[0].shape[0]):
      for j in range(self.validation_data[0].shape[1]):
        if numpy.sum(self.validation_data[0][i,j]) != 0:
        	yt.append(numpy.argmax(self.validation_data[1][i,j]))
        	yp.append(numpy.argmax(y_val_pred[i,j]))
    f1=f1_score(yt, yp, average='micro')  
    print('Epoch %d f1: %f' %(epoch, f1))
    print(classification_report(yt, yp, digits=4))
    self.f1_scores.append(f1)

    current = round(f1, 3)
    if current >= self.best:
      self.best = current
      self.wait = 0
      self.bestEpoch = epoch
      # Record the best weights if current results is better (less).
      self.best_weights = self.model.get_weights()
      self.model.save(self.modelTag) 
    else:
      self.wait += 1
      if self.wait >= self.patience:
        self.stopped_epoch = epoch
        self.model.stop_training = True
        print('Restoring model weights from the end of the best epoch: %s' % (str(self.bestEpoch)))
        self.model.set_weights(self.best_weights)

  def on_train_end(self, logs=None):
    if self.stopped_epoch > 0:
      print('Epoch %05d: early stopping' % (self.stopped_epoch + 1))

def getPredictions(X, Y, y_val_pred):
	yt = []
	yp = []
	for i in range(X.shape[0]):
		for j in range(X.shape[1]):
			if numpy.sum(X[i,j]) != 0:
				yt.append(numpy.argmax(Y[i,j]))
				yp.append(numpy.argmax(y_val_pred[i,j]))
	return yt, yp

def getMaxLen(featfile):
	maxlen = 0
	last_uid = ''
	with open(featfile) as fp:
		for line in fp:
			idd, feats, label, ref = line.strip().split(',')
			uid, unum = idd.rsplit('_', 1)
			if uid == last_uid:
				continue
			elif int(unum) > maxlen:
				maxlen = int(unum)

	return maxlen+1

def readFeatsForStd(featfile):
	xx = []
	yy = []
	with open(featfile) as fp:
		for line in fp:
			idd, feats, label, ref = line.strip().split(',')
			fvec = [float(x) for x in feats.split(' ')]
			xx.append(fvec)
			yy.append(int(label))

	return (numpy.array(xx), numpy.array(yy))

def prepFeats(featfile, maxlen, scaler, numclasses):
	num_samples = 0
	tmpfeats = ''
	lastid = ''
	with open(featfile) as fp:
		for line in fp:
			idd, feats, label, ref = line.strip().split(',')
			tmpfeats = feats
			if lastid != idd.rsplit('_', 1)[0]:
				lastid = idd.rsplit('_', 1)[0]
				num_samples = num_samples + 1

	featdim = len(tmpfeats.split(' '))

	xx = numpy.zeros([num_samples, maxlen, featdim])
	yy = numpy.zeros([num_samples, maxlen, numclasses+1])
	yy[:,:,0] = 1	
	rr = []
	last_uid = ''

	with open(featfile) as fp:
		i=0
		j=0
		rs = []
		for line in fp:
			idd, feats, label, ref = line.strip().split(',')
			fvec = [float(x) for x in feats.split(' ')]
			uid, unum = idd.rsplit('_', 1)
			if (last_uid != '') and (uid != last_uid):
				i = i+1
				j=0
				rr.append(rs)
				rs = []
			xx[i,j] = scaler.transform([fvec])[0]  
			yy[i,j,int(label)] = 1
			yy[i,j,0] = 0
			rs.append(ref)
			last_uid = uid
			j=j+1
		rr.append(rs)
	return (xx, yy)

import argparse

parser = argparse.ArgumentParser()

parser.add_argument('featsfile', type=str, help='Path to file with dev set sausage feats and labels')
parser.add_argument('outdir', type=str, help='Path to output model dir')

args = parser.parse_args()

if not os.path.isfile(args.featsfile):
    raise IOError(args.featsfile)

if not os.path.isdir(args.outdir):
    raise IOError(args.outdir)

numclasses=3

maxlen = getMaxLen(args.featsfile)
(trainX, trainY) = readFeatsForStd(args.featsfile)
scaler = StandardScaler()
scaler.fit(trainX)
pickle.dump(scaler, open(args.outdir + '/scaler.pkl','wb'))

(trainX_, trainY_) = prepFeats(args.featsfile, maxlen, scaler, numclasses)

indim=trainX_.shape[2]

print("Forming train-dev split from input feature set...")
trainX, devX, trainY, devY = train_test_split(trainX_, trainY_, test_size=0.33, shuffle= True)

model = Sequential()
model.add(Masking(mask_value=0., input_shape=(maxlen, indim)))
model.add(Bidirectional(LSTM(10, return_sequences=True), input_shape=(maxlen, indim)))
model.add(TimeDistributed(Dense(numclasses+1, activation='softmax')))

mTag = args.outdir + '/model.h5'
es=EarlyStoppingAtMaxF1(modelTag=mTag, patience=10)

model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

history = model.fit(trainX, trainY, validation_data=(devX, devY), epochs=200, batch_size=32, verbose=2, callbacks=[es])

model.save(mTag)