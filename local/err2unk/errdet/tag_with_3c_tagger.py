#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Copyright © 2020 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

import numpy, sys, os, pickle

from keras.models import Sequential, load_model
from keras.layers import Dense, LSTM, Bidirectional, TimeDistributed, Masking

from keras import backend as K

from sklearn.preprocessing import StandardScaler

from keras.models import model_from_json

def getMaxLen(featfile):
	maxlen = 0
	last_uid = ''
	with open(featfile) as fp:
		for line in fp:
			idd, feats, label, ref = line.strip().split(',')
			uid, unum = idd.rsplit('_', 1)
			if uid == last_uid:
				continue
			elif int(unum) > maxlen:
				maxlen = int(unum)

	return maxlen+1

def getPredictions(X, Y, y_val_pred):
	yt = []
	yp = []
	for i in range(X.shape[0]):
		_yp = []
		for j in range(X.shape[1]):
			if numpy.sum(X[i,j]) != 0:
				yt.append(numpy.argmax(Y[i,j]))
				_yp.append(numpy.argmax(y_val_pred[i,j]))
		yp.append(_yp)
	return yt, yp

def prepFeats(featfile, maxlen, scaler, numclasses):
	num_samples = 0
	tmpfeats = ''
	lastid = ''
	with open(featfile) as fp:
		for line in fp:
			idd, feats, label, ref = line.strip().split(',')
			tmpfeats = feats
			if lastid != idd.rsplit('_', 1)[0]:
				lastid = idd.rsplit('_', 1)[0]
				num_samples = num_samples + 1

	featdim = len(tmpfeats.split(' '))

	xx = numpy.zeros([num_samples, maxlen, featdim])
	yy = numpy.zeros([num_samples, maxlen, numclasses+1])
	yy[:,:,0] = 1
	rr = []
	last_uid = ''

	idLoL = []
	with open(featfile) as fp:
		i=0
		j=0
		rs = []
		ids = []
		for line in fp:
			idd, feats, label, ref = line.strip().split(',')
			fvec = [float(x) for x in feats.split(' ')]
			uid, unum = idd.rsplit('_', 1)
			if (last_uid != '') and (uid != last_uid):
				i = i+1
				j=0
				rr.append(rs)
				rs = []
				idLoL.append(ids)
				ids = []
			xx[i,j] = scaler.transform([fvec])[0]  
			yy[i,j,int(label)] = 1
			yy[i,j,0] = 0
			rs.append(ref)
			ids.append(idd)
			last_uid = uid
			j=j+1
		rr.append(rs)
		idLoL.append(ids)
	return (xx, yy, idLoL)

def printPreds(yp, ids):
	for i in range(len(yp)):
		if len(yp[i]) != len(ids[i]):
			print("Error: %d %d" % (len(yp[i]), len(ids[i])))
		print(ids[i][0].rsplit('_',1)[0], end=",")
		for j in range(len(yp[i])):
			print(yp[i][j],end='')
		print("\n")

def printPredsToFile(yp, ids, fpath):
	fp = open(fpath, "w")
	for i in range(len(yp)):
		if len(yp[i]) != len(ids[i]):
			print("Error: %d %d" %(len(yp[i]), len(ids[i])))
		fp.write(ids[i][0].rsplit('_',1)[0] + ", ")
		for j in range(len(yp[i])):                                                                 
			fp.write(str(yp[i][j]) + " ")
		fp.write("\n")

import argparse

parser = argparse.ArgumentParser()

parser.add_argument('featsfile', type=str, help='Path to file sausage feats for prediction')
parser.add_argument('modeldir', type=str, help='Path to error model dir')
parser.add_argument('out_preds', type=str, help='Path to output error predictions file')

args = parser.parse_args()

if not os.path.isfile(args.featsfile):
    raise IOError(args.featsfile)

if not os.path.isdir(args.modeldir):
    raise IOError(args.modeldir)

numclasses=3

scaler = pickle.load(open(args.modeldir + '/scaler.pkl','rb'))

maxlen = getMaxLen(args.featsfile)
(testX, testY, testIds) = prepFeats(args.featsfile, maxlen, scaler, numclasses)
indim=testX.shape[2]

new_model = Sequential()
new_model.add(Masking(mask_value=0., input_shape=(maxlen, indim)))
new_model.add(Bidirectional(LSTM(10, return_sequences=True), input_shape=(maxlen, indim)))
new_model.add(TimeDistributed(Dense(numclasses+1, activation='softmax')))

model = load_model(args.modeldir + '/model.h5')
for layer in new_model.layers:
	try:
		layer.set_weights(model.get_layer(name=layer.name).get_weights())
	except:
		print("Could not transfer weights for layer {}".format(layer.name))

print(new_model.summary())

pred = new_model.predict(testX, verbose=0)
yt, yp = getPredictions(testX, testY, pred)
printPredsToFile(yp, testIds, args.out_preds)

