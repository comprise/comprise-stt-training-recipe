#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Copyright © 2020 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

import sys, os, re, numpy
import kenlm

def getProb(lm, phrase, bosFlag):
	prob = 0
	for _prob,_,_ in lm.full_scores(phrase, bos=bosFlag, eos=False):
		prob = _prob
	return prob		

if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser()

	parser.add_argument('saus_dir', type=str, help='Path to sausage directory')
	parser.add_argument('saus_ref_align', type=str, help='Path to saus_best-bin.align file')
	parser.add_argument('arpaLM', type=str, help='Path to arpa LM file')
	parser.add_argument('graph_dir', type=str, help='Path to kaldi graph dir')
	parser.add_argument('out_saus_feats_n_labs', type=str, help='output file for features and labels from the confusion networks')

	args = parser.parse_args()

	words_file = args.graph_dir + "/words.txt"

	if not os.path.isfile(args.saus_ref_align):
		raise IOError(args.saus_ref_align)

	if not os.path.isdir(args.saus_dir):
		raise IOError(args.saus_dir)

	if not os.path.isfile(args.arpaLM):
		raise IOError(args.arpaLM)

	if not os.path.isfile(words_file):
		raise IOError(words_file)

	lm = kenlm.LanguageModel(args.arpaLM)

errorLabels ={}
vocab = []
refWords = {}

with open(words_file) as fp:
	for line in fp:
		line = line.rstrip()
		word, wid = line.split(' ');
		vocab.append(word)

with open(args.saus_ref_align) as fp:
	for line in fp:
		line = line.rstrip()
		labels = []
		refs = []
		if ' ' in line:
			uttid, aliText = line.split(' ', 1);
			alis = aliText.split(' ; ')
			for ali in alis:
				ref, hyp = ali.split(' ');
				if ref == hyp:				# correct word hyp
					labels.append(1)
					refs.append(ref)
				elif ref =='<eps>':			
					if hyp == '<heps>':		# non epsilon-only epsilon 'bins' from ASR 
						labels.append(2)	# no error
						refs.append(ref)
					else:
						labels.append(3)	# insertion error
						refs.append(ref)
				elif hyp == '<eps>':		# deletion error will not have a bin in CN and cannot be detected
					continue
				else:						# substitution error
					labels.append(3)
					refs.append(ref)
		else:
			uttid = line
		errorLabels[uttid] = labels 		# Note: label 0 reserved for padding 
		refWords[uttid] = refs

print("\nExtracting features from confusion networks and writing to %s" % (args.out_saus_feats_n_labs))

with open(args.out_saus_feats_n_labs,'w') as outfile:
	for r, d, f in os.walk(args.saus_dir):
		for file in f:
			m = re.search(r'saus\-wtimes\.\d+$', file)
			if m is not None:
				fnum = file.split('.')[-1]
				with open(os.path.join(r, file)) as fp, open(os.path.join(r, 'saut-wtimes.'+fnum)) as fp2: 
					for line in fp:
						line = line.replace('\n', ' ')

						uttid, binText = line.split(' ', 1)

						labels = errorLabels[uttid]
						refs = refWords[uttid]

						binText = binText.lstrip(' [')
						binText = binText.rstrip('] ')
						_bins = binText.split(' ] [ ')	

						bins = []
						binsT = []
						bid=0
						for bin in _bins:
							line2 = fp2.readline()
							if bin == '0 1':
								pass
							else:
								line2 = line2.replace('\n', ' ')
								uttid2, binTText = line2.split(' ', 1)
								binTUttid = uttid+'_'+str(bid)
								if uttid2 != binTUttid:
									print("Error: %s != %s" %(uttid2, binTUttid))
									sys.exit()
								binTText = binTText.strip(' ')
								_binsT = binTText.split(' ; ')	

								bins.append(bin)
								binsT.append(_binsT)

							bid = bid + 1

						if (len(labels) == 0) or (len(bins) == 0):
							continue
							
						if (len(labels) != len(bins)):
							print("Error: %s %d != %d %d" %(uttid, len(labels), len(bins), len(binsT)))
							print(binText)
							print(labels)
							sys.exit()

						j=0
						for bin, binT in zip(bins, binsT):
							arcs = bin.split(' ')
							featvec = [] 

							tmpArr = numpy.zeros(len(binT))
							for bti in range(0, len(binT)):
								bbeg, bend = binT[bti].split(' ')
								ff = float(bend) - float(bbeg)
								tmpArr[bti] = ff
							ff19 = numpy.mean(tmpArr)
							ff20 = numpy.std(tmpArr)

							ff = tmpArr[0]
							featvec.append(ff) 						#1 bin duration

							### feature reference: https://www.sri.com/wp-content/uploads/pdf/asr_error_detection_using_recurrent_neural_network_languag.pdf
							ff = numpy.log10(float(arcs[1]))		#2 log word posterior logp(wj|X)
							featvec.append(ff) 		
						
							ww = vocab[int(arcs[0])]
							if ww == '<eps>':
								ff = 0.0
							else:
								ff = getProb(lm, ww, False)			#3 log unigram probability p(wj)
							featvec.append(ff)

							phrase = vocab[int(arcs[0])]
							if phrase == '<eps>':
								if j+1 < len(bins):
									tmpArcs = bins[j+1].split(' ')
									phrase = vocab[int(tmpArcs[0])]
								else:
									phrase = ''
							bosFlag = False
							k = 1
							hist = 3
							while k <= hist:
								if j-k >= 0:
									tmpArcs = bins[j-k].split(' ')
									if vocab[int(tmpArcs[0])] == '<eps>':
										hist = hist + 1
									else:
										phrase =  vocab[int(tmpArcs[0])] + " " + phrase
								else:
									bosFlag = True
								k = k+1

							ff = getProb(lm, phrase, bosFlag)		#4 log forward 4 gram probability p(wj|wj-1 wj-2 wj-3)
							featvec.append(ff) 		

							ff = (j+1) * 1.0 / len(bins)			#5 relative word position j/None
							featvec.append(ff) 		

							ff = numpy.log10(len(bins))				#6 log sentence length log N
							featvec.append(ff) 		

							ff = numpy.log10(len(arcs)/2)			#7 number of alternative word candidates in a confusion slot
							featvec.append(ff) 						

							if j-1 >= 0:							#8 log word posterior of the previous word logp(wj-1|X)
								tmpArcs = bins[j-1].split(' ')
								ff = numpy.log10(float(tmpArcs[1]))
							else:
								ff = -10.0
							featvec.append(ff) 		

							if j+1 < len(bins):						#9 log word posterior of the next word log p(wj+1|X)
								tmpArcs = bins[j+1].split(' ')
								ff = numpy.log10(float(tmpArcs[1]))	
							else:
								ff = -10.0
							featvec.append(ff) 		

							if j-2 >= 0:							#10 log word posterior of the previous word but one logp(wj-2|X)
								tmpArcs = bins[j-2].split(' ')
								ff = numpy.log10(float(tmpArcs[1]))
							else:
								ff = -10.0
							featvec.append(ff) 		

							if j+2 < len(bins):						#11 log word posterior of the next word but one log p(wj+2|X)
								tmpArcs = bins[j+2].split(' ')
								ff = numpy.log10(float(tmpArcs[1]))
							else:
								ff = -10.0
							featvec.append(ff) 		

							vals = numpy.array(list(map(float, arcs[1::2])))
							ff = -1*numpy.sum(vals*numpy.log(vals))		#12 local entropy of word posteriors in a confusion slot
							featvec.append(ff) 		
							ff = numpy.std(vals)					#13 standard deviation of word posteriors in a confusion slot
							featvec.append(ff) 		

							if j-1 >= 0:
								tmpArcs = bins[j-1].split(' ')
								if vocab[int(tmpArcs[0])] == '<eps>':
									ff = 1							#14 is the previous word equal to <eps>, a null symbol corresponding to word deletion?
								else:
									ff = 0
							else:
								ff = 0
							featvec.append(ff) 		

							if j+1 < len(bins):
								tmpArcs = bins[j+1].split(' ')		
								if vocab[int(tmpArcs[0])] == '<eps>':
									ff = 1							#15 is the next word equal to <eps>, a null symbol corresponding to word deletion?
								else:
									ff = 0
							else:
								ff = 0
							featvec.append(ff) 		

							ff = numpy.log10(len(vocab[int(arcs[0])]))	#16 log length of the current word wj
							featvec.append(ff) 		

							featvec.append(ff19) 
							featvec.append(ff20) 

							fvecstr = "%s_%d,%s,%d,%s" %(uttid, j, " ".join(map(str, featvec)).strip(), labels[j], refs[j])

							outfile.write(fvecstr+"\n")
							j = j+1

	outfile.close()

