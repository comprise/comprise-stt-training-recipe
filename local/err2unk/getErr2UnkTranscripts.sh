#!/bin/bash

# Copyright © 2020 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

unksym="<unk>"

echo ""	>/dev/stderr
echo "$0 $@"  >/dev/stderr # Print the command line for logging
echo "" >/dev/stderr

if [ "$#" -ne 3 ]; then
	echo ""
	echo "Usage: $0 saus_dir graph_dir unsup_error_preds"
	echo ""
	exit 1;
fi

[ -f ./path.sh ] && . ./path.sh
. parse_options.sh || exit 1;

saudir=$1
preds=$3
symtab=$2/words.txt

for f in $symtab $preds $saudir/../num_jobs; do
  [ ! -f $f ] && echo "$0: no such file $f" && exit 1;
done

! ls $saudir/saus-wtimes.* >/dev/null &&\
  echo "$0: No sausage files in input directory $sausdir" && exit 1;

unkid=`cat $symtab | grep -i $unksym | cut -d" " -f2`

nj=`cat $saudir/../num_jobs`
for (( j=1; j<=$nj; j++ ))
do  
   perl local/err2unk/getErr2UnkTranscripts.pl $saudir"/saus-wtimes."$j $preds $unkid | utils/int2sym.pl -f 2- $symtab
done
