#!/bin/bash

# Copyright © 2020 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

. ./path.sh  # set paths to binaries

echo ""
echo "$0 $@"  # Print the command line for logging
echo ""

if [ $# != 3 ]; then
	echo ""
	echo "USAGE: $0 saus_dir graph_dir ref_text"
	echo ""
	exit 1;
fi

sausdir=$1
graphdir=$2
reftext=$3

saushyp=${sausdir}/saus_bin-best_with-heps.hyp 
out="${saushyp}.align"

for f in $reftext $graphdir/words.txt; do
  [ ! -f $f ] && echo "$0: no such file $f" && exit 1;
done

! ls $sausdir/saus-wtimes.* >/dev/null &&\
  echo "$0: No sausage files in input directory $sausdir" && exit 1;

[ ! -f local/err2unk/saus-to-hyp.pl ] && echo "Cannot find local/err2unk/saus-to-hyp.pl! Are you calling this script from a different location?" && exit 1;

echo "Aligning sausage best hypothesis to reference and saving to $out ..."
echo ""

perl local/err2unk/saus-to-hyp.pl ${sausdir} | int2sym.pl -f 2- ${graphdir}/words.txt | sed 's/<eps>/<heps>/g' > $saushyp

align-text ark:$reftext ark:$saushyp ark:$out || exit 1;

echo "Completed successfully!"
exit 0;
