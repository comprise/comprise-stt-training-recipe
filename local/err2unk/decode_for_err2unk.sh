#!/bin/bash

# Copyright © 2021 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

set -e

stage=0
split=""
dir=exp/chain/tdnn
mfccdir=mfcc_hires

[ -f ./path.sh ] && . ./path.sh
. parse_options.sh || exit 1;

if [ -z "$split" ]; then
	echo "split is not set !"
	exit 1;
fi

if [ $stage -le 0 ] || [ ! -f data/$split/feats.scp ]; then
	steps/make_mfcc.sh --nj 6 --mfcc-config conf/mfcc_hires.conf data/${split} exp/make_hires/$split $mfccdir;
	
	steps/compute_cmvn_stats.sh data/${split} exp/make_hires/${split} $mfccdir;
fi

if [ $stage -le 0 ] || [ ! -d exp/nnet3/ivectors_${split} ]; then
	steps/online/nnet2/extract_ivectors_online.sh --nj 6 data/${split} exp/nnet3/extractor exp/nnet3/ivectors_${split} || exit 1;
fi

if [ $stage -le 1 ]; then
	steps/nnet3/decode_lookahead.sh --nj 6 \
    	--skip_scoring true \
    	--acwt 1.0 --post-decode-acwt 10.0 \
    	--online-ivector-dir exp/nnet3/ivectors_${split} \
    	${dir}/graph data/${split} ${dir}/decode_for_e2u_${split}
fi
