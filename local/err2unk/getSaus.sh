#!/bin/bash

# Copyright © 2020 INRIA (Imran Sheikh) 
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

final_mdl=""

echo ""
echo "$0 $@"  # Print the command line for logging
echo ""

[ -f ./path.sh ] && . ./path.sh
. parse_options.sh || exit 1;

if [ $# != 3 ]; then
	echo ""
	echo "USAGE: $0 [options] lattice_dir graph_dir lm_wt"
	echo ""	
	echo "options: --final_mdl path_to_final.mdl"
	echo ""
	exit 1;
fi

latdir=$1
graphdir=$2
lmwt=$3

for f in $latdir/num_jobs $graphdir/phones/word_boundary.int; do
  [ ! -f $f ] && echo "$0: no such file $f" && exit 1;
done

[ -z "${final_mdl}" ] && final_mdl="$latdir/../final.mdl"
[ ! -f ${final_mdl} ] && echo "$0: no such file ${final_mdl}" && echo "Try with --final_mdl option ..." && exit 1;

! ls $latdir/lat.*.gz >/dev/null &&\
  echo "$0: No lattices in input directory $latdir" && exit 1;

nj=`cat $latdir/num_jobs`
ascale=`echo "1 / ( $lmwt )" | bc -l`

mkdir -p $latdir/sau

echo "Extracting confusion networks aka sausages to $latdir/sau ..."

run.pl JOB=1:$nj $latdir/sau/log/gen-saus.JOB.log \
lattice-align-words $graphdir/phones/word_boundary.int \
     				${final_mdl} \
     				ark:"gunzip -c $latdir/lat.JOB.gz |" ark:- \| \
lattice-mbr-decode-wtimes --acoustic-scale=$ascale ark:- ark,t:/dev/null ark:/dev/null ark,t:$latdir/sau/saus-wtimes.JOB ark,t:$latdir/sau/saut-wtimes.JOB || exit 1;

echo "Completed successfully!"
exit 0;

