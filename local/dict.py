#!/usr/bin/env python3

from collections import defaultdict
import sys
import re

infile = sys.stdin

countPairs = False
if len(sys.argv) > 1:
    countPairs = (sys.argv[1] == '-p')

if countPairs:
    pairs = defaultdict(int)
    wordidx = dict()
    wordidxinv = list()
    lineidx = 0
    for line in infile:
        lineidx += 1
        tset = set(t for t in line.rstrip().split(' ') if t)
        for t in tset:
            if not t in wordidx:
                idx = len(wordidx)
                wordidx[t] = idx
                wordidxinv.append(t)
        for t1 in tset:
            for t2 in tset:
                i1 = wordidx[t1]
                i2 = wordidx[t2]
                print ("%d\t%d" % (i1, i2))

else:
    frequencies = defaultdict(int)
    for line in infile:
        for t in line.rstrip().split(' '):
            if t:
                frequencies[t] += 1
    
    for t in frequencies:
        print ("%s\t%d" % (t, frequencies[t]))
