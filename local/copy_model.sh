#!/bin/sh

cd /recipe

echo "copying shared model"

# package component
mkdir -p /components
mkdir -p /model

for i in graph_lookahead_arpa graph_lookahead lm_arpa tdnn tdnn_online ; do
  if [ -f /data/deps/$i ]; then
    cp -r /data/deps/$i /components/$i
    tar -xzf /data/deps/$i -C /
  fi
done

cd /model
# create softlink for compatibility 
ln -s graph_lookahead graph

tar -czf /model.mdl /model

# empty validation
touch /validation.tgz
