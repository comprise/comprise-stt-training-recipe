#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Copyright © 2021 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

import sys, os
import rhasspynlu

if __name__ == '__main__':
	import argparse

	parser = argparse.ArgumentParser()
	parser.add_argument('gram_ini_file', type=argparse.FileType('r'), help='Path to grammar file')
	parser.add_argument('out_dir', type=str, help='output directory')
	args = parser.parse_args()

	if not os.path.isdir(args.out_dir):
		raise IOError(args.out_dir)

	# Load grammar, parse, convert to FST and write FST
	gram = rhasspynlu.parse_ini(args.gram_ini_file)
	gram_graph = rhasspynlu.intents_to_graph(gram)
	gram_fst = rhasspynlu.graph_to_fst(gram_graph)
	gram_fst.write_fst(os.path.join(args.out_dir, "gram_fst.txt"), 
						os.path.join(args.out_dir, "gram_in_symbols.txt"), 
						os.path.join(args.out_dir, "gram_out_symbols.txt"))