#!/bin/bash

# Copyright © 2021 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

echo ""
echo "$0 $@"  # Print the command line for logging
echo ""

if [ $# != 3 ]; then
	echo ""
	echo "USAGE: $0 grammar_ini_file bg_arpa_file out_arpa_dir"
	echo ""
	exit 1;
fi

gram_file=$1
bg_arpa=$2
out_dir=$3

tmp_dir="/tmp" # can be changed to any other tmp directory

for f in ${gram_file} ${bg_arpa}; do
  [ ! -f $f ] && echo "$0: no such file $f" && exit 1;
done

mix_coeff="0.1"

# try to parse mix_coeff from gram_file
pattern="mix_coeff="
head_flag=`head -1 ${gram_file} | grep $pattern`
if [ ! -z ${head_flag} ]; then
	mix_coeff=`head -1 ${gram_file} | cut -d"=" -f2`
fi

if [ $(echo "${mix_coeff}>=1.0"| bc) -eq 1 ]; then
	echo "Invalid value mix_coeff=${mix_coeff}. Changing to mix_coeff=0.1"
	mix_coeff="0.1"
fi

if [ $(echo "${mix_coeff}<0.0"| bc) -eq 1 ]; then
	echo "Invalid value mix_coeff=${mix_coeff}. Changing to mix_coeff=0.1"
	mix_coeff="0.1"
fi

# grammar to FST
python3.7 local/gram/gram_to_fst.py ${gram_file} ${tmp_dir}
fstcompile --keep_isymbols --keep_osymbols --isymbols=${tmp_dir}/gram_in_symbols.txt --osymbols=${tmp_dir}/gram_out_symbols.txt ${tmp_dir}/gram_fst.txt ${tmp_dir}/gram.fst

# grammar FST to counts
ngramcount ${tmp_dir}/gram.fst ${tmp_dir}/gram.fst.counts

# grammar counts to ngram model
ngrammake --method=witten_bell ${tmp_dir}/gram.fst.counts ${tmp_dir}/gram.fst.ngram # WB is better for smaller data

if [ $(echo "${mix_coeff}==0.0"| bc) -eq 1 ]; then
	echo "Found mix_coeff=0.0, saving arpa equivalent of grammar ..." 
	ngramprint --ARPA ${tmp_dir}/gram.fst.ngram ${out_dir}/gram_bg-arpa_merge.arpa
else
	echo "mix_coeff=${mix_coeff}, mixing arpa with grammar ..." 

	# background arpa LM to FST
	ngramread --ARPA ${bg_arpa} ${tmp_dir}/bg_lm.fst

	# mix gram ngram model and arpa FST
	ngrammerge --alpha=${mix_coeff} ${tmp_dir}/bg_lm.fst ${tmp_dir}/gram.fst.ngram ${tmp_dir}/gram_bg-arpa.merge

	# merged model to arpa
	ngramprint --ARPA ${tmp_dir}/gram_bg-arpa.merge ${out_dir}/gram_bg-arpa_merge.arpa
fi

echo "Arpa mixing completed..."
echo ""
exit 0;