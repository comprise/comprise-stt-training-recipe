#!/bin/bash

# Derived software, Copyright © 2020 INRIA (Imran Sheikh) 
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
#
#
# Based on Kaldi (kaldi/egs/wsj/s5/local/wsj_format_local_lms.sh), Copyright 2019 © Johns Hopkins University (author: Daniel Povey, Guoguo Chen)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)
# https://github.com/kaldi-asr/kaldi/blob/master/COPYING

lang=data/lang
datadir=

echo "$0 $@"  # Print the command line for logging
. ./path.sh
. utils/parse_options.sh || exit 1;

lm_srcdir_3g=$datadir/3gram

[ ! -d "$lm_srcdir_3g" ] && echo "No such dir $lm_srcdir_3g" && exit 1;

for d in $datadir/lang_test_tg; do
  rm -r $d 2>/dev/null
  cp -r $lang $d
done

# Check a few files that we have to use.
for f in words.txt oov.int; do
  if [[ ! -f $lang/$f ]]; then
    echo "$0: no such file $lang/$f"
    exit 1;
  fi
done

# KenLM expects tabs to be used in ARPA instead of spaces
# therefore we need to re-format the LM
gunzip $lm_srcdir_3g/lm_unpruned.gz
sed -i "s; ;\t;g" $lm_srcdir_3g/lm_unpruned
sed -i "s;ngram\t;ngram ;g" $lm_srcdir_3g/lm_unpruned
gzip $lm_srcdir_3g/lm_unpruned

gunzip -c $lm_srcdir_3g/lm_unpruned.gz | \
  arpa2fst --disambig-symbol=#0 \
           --read-symbol-table=$lang/words.txt - $datadir/lang_test_tg/G.fst || exit 1;
  fstisstochastic $datadir/lang_test_tg/G.fst

exit 0;

