#!/bin/bash

# Copyright © 2021 INRIA (Imran Sheikh)
# Apache 2.0  (http://www.apache.org/licenses/LICENSE-2.0)

localdir=local/
mdldir=exp/chain/tdnn
graphdir=exp/chain/tdnn/graph
arpagz=/data/kaldi-data-transcribed/3gram/lm_unpruned.gz
e2udir=exp/e2u/
outdir=data/train_e2u_semisup

###  Prepare for STT Error Detection; as per Step 2 of https://gitlab.inria.fr/comprise/speech-to-text-weakly-supervised-learning#err2unk-based-training
split=dev
latticedir=${mdldir}/decode_for_e2u_${split}
bash ${localdir}/err2unk/decode_for_err2unk.sh --stage 1 --split ${split} # stage=1 assumes dev mfcc_hires and ivector features already extracted
# set lmwt 
if [ -f ${mdldir}/lm_wt ]; then
    lmwt=`cat ${mdldir}/lm_wt`
else
    lmwt=10
fi
bash ${localdir}/err2unk/getSaus.sh ${latticedir} ${graphdir} ${lmwt}

split=train_unlab
latticedir=${mdldir}/decode_for_e2u_${split} 
utils/copy_data_dir.sh /data/kaldi-data-untranscribed data/${split}
utils/fix_data_dir.sh data/${split}
bash ${localdir}/err2unk/decode_for_err2unk.sh --stage 0 --split ${split}
bash ${localdir}/err2unk/getSaus.sh ${latticedir} ${graphdir} ${lmwt}

### Train STT Error Detector; as per Step 3 of https://gitlab.inria.fr/comprise/speech-to-text-weakly-supervised-learning#err2unk-based-training
split=dev
latticedir=${mdldir}/decode_for_e2u_${split}
sausdir=${latticedir}/sau
bash ${localdir}/err2unk/sausAlign.sh ${sausdir} ${graphdir} data/${split}/text
mkdir -p ${e2udir}/errmdl
python3.7 ${localdir}/err2unk/errdet/saus_feats_for_train.py ${sausdir} ${sausdir}/saus_bin-best_with-heps.hyp.align ${arpagz} ${graphdir} ${e2udir}/dev_saus_feats_n_labs
python3.7 ${localdir}/err2unk/errdet/train_3c_error_tagger_on_dev.py ${e2udir}/dev_saus_feats_n_labs ${e2udir}/errmdl


### Get unsupervised speech transcripts; as per Step 4 of https://gitlab.inria.fr/comprise/speech-to-text-weakly-supervised-learning#err2unk-based-training
split=train_unlab
latticedir=${mdldir}/decode_for_e2u_${split} 
sausdir=${latticedir}/sau
python3.7 ${localdir}/err2unk/errdet/saus_feats_for_predict.py ${sausdir} ${arpagz} ${graphdir} ${e2udir}/unsup_saus_feats
python3.7 ${localdir}/err2unk/errdet/tag_with_3c_tagger.py ${e2udir}/unsup_saus_feats ${e2udir}/errmdl ${e2udir}/unsup_error_preds
bash ${localdir}/err2unk/getErr2UnkTranscripts.sh ${sausdir} ${graphdir} ${e2udir}/unsup_error_preds > data/$split/e2u_text


### Prepare e2u semi-supervised data for training new models; as per Step 5 of https://gitlab.inria.fr/comprise/speech-to-text-weakly-supervised-learning#err2unk-based-training
bash ${localdir}/err2unk/prepareNewDataDir.sh data/train_unlab/e2u_text data/train /data/kaldi-data-untranscribed ${outdir}

