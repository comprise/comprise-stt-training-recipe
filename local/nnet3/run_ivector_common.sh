#!/bin/bash

. ./cmd.sh
set -e
stage=1
train_stage=-10
generate_alignments=true # false if doing ctc training
speed_perturb=true
gmm=exp/tri3-seed/tri3_full
lang=data/lang

nnet3_affix=
ali_dir=exp/tri4_ali

. ./path.sh
. ./utils/parse_options.sh

gmm_dir=${gmm}
mkdir -p nnet3 # where is this used? to remove if not being used 

# perturbed data preparation
train_set=$1
if [ "$speed_perturb" == "true" ]; then

  if [ $stage -le 1 ]; then
    #Although the nnet will be trained by high resolution data, we still have to perturbe the normal data to get the alignment
    # _sp stands for speed-perturbed

    for datadir in $train_set; do
      utils/data/perturb_data_dir_speed_3way.sh ${datadir} ${datadir}_sp

      mfccdir=mfcc_perturbed
      steps/make_mfcc.sh --cmd "$train_cmd" --nj 4 \
        ${datadir}_sp exp/make_mfcc/${datadir}_sp $mfccdir || exit 1;
      steps/compute_cmvn_stats.sh ${datadir}_sp exp/make_mfcc/${datadir}_sp $mfccdir || exit 1;
      utils/fix_data_dir.sh ${datadir}_sp
    done
  fi

  if [ $stage -le 2 ] && [ "$generate_alignments" == "true" ]; then
    #obtain the alignment of the perturbed data
    echo "$0: aligning with the perturbed low-resolution data"
    steps/align_fmllr.sh --nj 20 --cmd "$train_cmd" \
        ${train_set}_sp $lang $gmm_dir $ali_dir || exit 1
  fi
  train_set=${train_set}_sp
fi

if [ $stage -le 3 ]; then
  mfccdir=mfcc_hires

  # create hires versions of train_set
  for dataset in $train_set; do
    utils/copy_data_dir.sh $dataset ${dataset}_hires

    # scale the waveforms, this is useful as we don't use CMVN
    data_dir=${dataset}_hires

    # do volume-perturbation on the training data prior to extracting hires
    # features; this helps make trained nnets more invariant to test data volume.
    utils/data/perturb_data_dir_volume.sh ${data_dir} || exit 1;

    steps/make_mfcc.sh --nj 6 --mfcc-config conf/mfcc_hires.conf \
        --cmd "$train_cmd" ${dataset}_hires exp/make_hires/$dataset $mfccdir;
    steps/compute_cmvn_stats.sh ${dataset}_hires exp/make_hires/${dataset} $mfccdir;

    # Remove the small number of utterances that couldn't be extracted for some
    # reason (e.g. too short; no such file).
    utils/fix_data_dir.sh ${dataset}_hires;
  done

fi

# ivector extractor training
if [ $stage -le 5 ]; then
  mkdir -p exp/nnet3${nnet3_affix}/diag_ubm
  temp_data_root=exp/nnet3${nnet3_affix}/diag_ubm

  num_utts_total=$(wc -l <${train_set}_hires/utt2spk)
  num_utts=$[$num_utts_total/4]
  utils/data/subset_data_dir.sh ${train_set}_hires \
     $num_utts ${temp_data_root}/${train_set}_hires_subset

  echo "$0: computing a PCA transform from the hires data."
  steps/online/nnet2/get_pca_transform.sh --cmd "$train_cmd" \
      --splice-opts "--left-context=3 --right-context=3" \
      --max-utts 10000 --subsample 2 \
       ${temp_data_root}/${train_set}_hires_subset \
       exp/nnet3${nnet3_affix}/pca_transform

fi

if [ $stage -le 6 ]; then
  echo "$0: training the diagonal UBM."
  # Use 512 Gaussians in the UBM.
  steps/online/nnet2/train_diag_ubm.sh --cmd "$train_cmd" --nj 6 \
    --num-frames 700000 \
    --num-threads 8 \
    ${temp_data_root}/${train_set}_hires_subset 512 \
    exp/nnet3${nnet3_affix}/pca_transform exp/nnet3${nnet3_affix}/diag_ubm

fi

if [ $stage -le 7 ]; then
  steps/online/nnet2/train_ivector_extractor.sh --cmd "$train_cmd" --nj 6 \
    ${train_set}_hires exp/nnet3${nnet3_affix}/diag_ubm exp/nnet3${nnet3_affix}/extractor || exit 1;
fi

if [ $stage -le 8 ]; then

  # having a larger number of speakers is helpful for generalization, and to
  # handle per-utterance decoding well (iVector starts at zero).
  utils/data/modify_speaker_info.sh --utts-per-spk-max 2 \
    ${train_set}_hires ${train_set}_hires_max2

  steps/online/nnet2/extract_ivectors_online.sh --cmd "$train_cmd" --nj 4 \
    ${train_set}_hires_max2 exp/nnet3${nnet3_affix}/extractor exp/nnet3${nnet3_affix}/ivectors || exit 1;
fi

exit 0;
