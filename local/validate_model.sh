#!/usr/bin/env bash

set -e

am=exp/chain/tdnn
testset=dev
nnet3_affix=
suffix=
stage=-1
mfccdir=mfcc_hires

. ./path.sh
. ./utils/parse_options.sh

if [ $stage -le -1 ]; then
	steps/make_mfcc.sh --nj 6 --mfcc-config conf/mfcc_hires.conf \
        data/${testset} exp/make_hires/$testset $mfccdir;
	steps/compute_cmvn_stats.sh data/${testset} exp/make_hires/${testset} $mfccdir;
fi

steps/online/nnet2/extract_ivectors_online.sh --nj 6 \
    data/${testset} exp/nnet3${nnet3_affix}/extractor exp/nnet3${nnet3_affix}/ivectors_${testset} || exit 1;

# Decode with runtime composition
steps/nnet3/decode_lookahead.sh --nj 6 \
    --skip_scoring true \
    --acwt 1.0 --post-decode-acwt 10.0 \
    --online-ivector-dir exp/nnet3${nnet3_affix}/ivectors_${testset} \
    ${am}/graph data/${testset} ${am}/decode_${testset}_lookahead_arpa

steps/scoring/score_kaldi_wer.sh data/$testset $am/graph $am/decode_${testset}_lookahead_arpa

lm_wt=`rev $am/decode_${testset}_lookahead_arpa/scoring_kaldi/best_wer | cut -d "_" -f2 | rev`
echo "$lm_wt" > ${am}/lm_wt

tar -czf /validation${suffix}.tgz $am/decode_${testset}_lookahead_arpa/scoring_kaldi/best_wer 
