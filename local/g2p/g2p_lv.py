#!/usr/bin/python
# -*- coding: utf-8 -*-

import re
import sys
import codecs
import argparse
import subprocess
import os


class G2PConverter:
    abbr_phone_map = {"a":"AA", u"ā":u"AA","b":"B EE", "c":"C EE", u"č":u"CH EE","d":"D EE", "e":"EE", u"ē":u"EE", "f":"E F", "g":"G AA", u"ģ":u"GJ EE", "h":"H AA", "i":"II", u"ī":u"II", "j":"J EE", "k":"K AA", u"ķ":u"KJ EE", "l":"E L", u"ļ":u"E LJ","m":"E M", "n":"E N", u"ņ":u"E NJ", "o":"O", "p":"P EE", "r":"E R", "s":"E S", u"š":u"E SH", "t":"T EE", "u":"UU", u"ū":u"UU", "v":"V EE", "z":"Z EE", u"ž":u"ZH EE", "w":"V EE", "x":"I K S", "y":"II", "q":"K UU"}
    phone_to_graph_map = {"A":"a", "AA":"ā", "B":"b", "C":"c", "CH":"č", "D":"d", "E":"e", "EE":"ē", "F":"f", "G":"g", "GJ":"ģ", "H":"h", "I":"i", "II":"ī", "J":"j", "K":"k", "KJ":"ķ", "L":"l", "LJ":"ļ", "M":"m", "N":"n", "NJ":"ņ", "O":"o", "P":"p", "R":"r", "S":"s", "SH":"š", "T":"t", "U":"u", "UU":"ū", "V":"v", "Z":"z", "ZH":"ž", "IE":"ie","AI":"ai"}

    letters = set(u'aābcčdeēfgģhiījkķlļmnņoóōprsštuūvzž' + u'xwqy' + u'EĒ')
    transform = {
            u'ā': 'aa',
            u'ē': 'ee',
            u'ī': 'ii',
            u'ū': 'uu',
            u'č': 'ch',
            u'š': 'sh',
            u'ž': 'zh',
            u'ģ': 'gj',
            u'ķ': 'kj',
            u'ļ': 'lj',
            u'ņ': 'nj',
            u'x': 'k s',
            u'q':'k',
            u'y':'i',
            u'w': 'v',
            }
    diphtongs = set()

    def __init__(self, args):
        self.excp_list = list()
        self.excp_pron = list()
        self.prondict = dict()
        self.dir = os.path.dirname(os.path.realpath(__file__))
        self.multiword = args.multiword
    
    def phoneme_to_grapheme(self,phones):
        phone_list = phones.split()
        result = list()
        for phone in phone_list:
            result.append(self.phone_to_graph_map[phone])
        return result
    
    def loadExceptions(self,excp_file):
        self.excp_list = [tmp.decode('utf-8').rstrip("\n").split("\t")[0] for tmp in open(excp_file,"r").readlines()]
        self.excp_pron = [" ".join(tmp.decode('utf-8').rstrip("\n").split("\t")[1:]) for tmp in open(excp_file,"r").readlines()]
        
    def isAbbr(self,token):
        result = True
        for t in token:
            if (t.isupper() == False and t != '+'):
                result = False
                break
        return result
    
    def isInExcpList(self,token):
        if token in self.excp_list:
            return True
        if (token.replace("+","") in self.excp_list):
            return True
        else:
            return False
    
    def convertExcpFromList(self,excp):
            if excp in self.excp_list:
                return self.excp_pron[self.excp_list.index(excp)]
            else:
                return self.excp_pron[self.excp_list.index(excp.replace("+",""))]
    
    def convertAbbr(self,token):
        result = list()        
        for grapheme in token:
            grapheme = grapheme.lower()
            if grapheme in self.abbr_phone_map:
                result.append(self.abbr_phone_map[grapheme])
        return result
        
    def load_phondict(self, path):
        for line in codecs.open(path, 'r', 'utf-8'):
            line = line.rstrip().split()
            if len(line) == 2:
                (graph, phon) = line
                self.prondict[graph] = list(phon)
            else:
                self.prondict[line[0]] = line[1:]

    def transform_upper(self, graphemes):
        for c in graphemes:
            yield c.upper()

    def transform_simple(self, graphemes):
        graphemes = list(graphemes)
        last_i = len(graphemes) - 1
        i = -1
        for c in graphemes:
            i += 1
            if self.multiword and i > 0 and i < last_i and c == "-":
                yield "SL_SIL"
                continue
            if not c in self.letters:
                continue
            if c in self.transform:
                yield self.transform[c]
                continue
            yield c


    def transform_ts(self, graphemes):
        c_last = ''
        for c in graphemes:
            c2 = c_last + c
            if c2 == 'ts':
                yield 'c'
                c_last = ''
            else:
                if c_last:
                      yield c_last
                c_last = c
        if c_last:
            yield c_last
    

    def transform_diphtongs(self, graphemes):
        c_last = ''
        for c in graphemes:
            c2 = c_last + c
            if c2 in self.diphtongs:
                yield c2
                c_last = ''
            else:
                if c_last:
                    yield c_last
                c_last = c
        if c_last:
            yield c_last

    def transform_pb(self, graphemes):
        grstring = ''.join([c for c in graphemes])
        for c in grstring:
            yield c

    def convert(self, graphemes):
        if graphemes and graphemes[0] == '<':
            return ['++' + graphemes[1:-1].upper() + '++']
        if graphemes in self.prondict:
            return self.prondict[graphemes]
        result = graphemes.lower()
        result = self.transform_simple(result)
        result = self.transform_diphtongs(result)
        result = self.transform_upper(result)
        result = list(result)
        if result[0] == "SL_SIL":
            result = result[1:]
        if result[-1] == "SL_SIL":
            result = result[:-1]
        return result

class G2PConverterLT(G2PConverter):
	abbr_phone_map = {"a":"A", u"ą":u"AE","b":"B EE", "c":"C EE", u"č":u"CH EE","d":"D EE", "e":"EE", u"ė":u"EE", "f":"E F", "g":"G EE", u"ę":u"AE", "h":"H A", "i":"I", u"į":u"II", "j":"J O", "k":"K A", u"ų":u"UU", "l":"E L", "m":"E M", "n":"E N", u"ņ":u"N J", "o":"O", "p":"P EE", "r":"E R", "s":"E S", u"š":u"E SH", "t":"T EE", "u":"UU", u"ū":u"UU", "v":"V EE", "z":"Z EE", u"ž":u"ZH EE", "w":"V EE", "x":"I K S", "y":"II", "q":"K UU"}
	phone_to_graph_map = {"A":"a", "AA":"ā", "B":"b", "C":"c", "CH":"č", "D":"d", "E":"e", "EE":"ē", "F":"f", "G":"g", "GJ":"ģ", "H":"h", "I":"i", "II":"ī", "J":"j", "K":"k", "KJ":"ķ", "L":"l", "LJ":"ļ", "M":"m", "N":"n", "NJ":"ņ", "O":"o", "P":"p", "R":"r", "S":"s", "SH":"š", "T":"t", "U":"u", "UU":"ū", "V":"v", "Z":"z", "ZH":"ž", "IE":"ie","AI":"ai"}

	letters = set(u'aąbcčdeęėfghiįyjklmnoprsštuųūvzž' + u'xwq')
	transform = {		
			u'ą': 'a',
			u'ę': 'ae',
			u'ė': 'ee',						
			u'į': 'ii',
			u'ū': 'uu',
			u'č': 'ch',
			u'š': 'sh',
			u'ž': 'zh',
			u'ų': 'uu',
			u'x': 'k s',
                        u'ņ': 'n j',
			u'q':'k',
			u'y':'ii',
			u'w': 'v',
			}
        diphtongs = set()

            
parser = argparse.ArgumentParser(description='Grapheme-to-phoneme converter')
parser.add_argument('-d', '--dict', help='load pronunciation dictionary')
parser.add_argument('-f', '--fillers', help='convert fillers to phonemes',
            action='store_const', const=True, default = False)
parser.add_argument('-s', '--sphinx', help='produce dictionary in Sphinx format', 
                        action='store_const', const=True, default = False)
parser.add_argument('-e', '--excpt', help='include exception file with words, that are pronounced specially. will be print in the beggining')
parser.add_argument('-p', '--phone', help='convert from phonemes to graphemes', 
                        action='store_const', const=True, default = False)
parser.add_argument('-m', '--multiword', help='insert SL_SIL between words in multiword tokens', 
                        action='store_const', const=True, default = False)
parser.add_argument('-o', '--one', help='input file contains one word per line, i.e. that is a dictionary', 
                        action='store_const', const=True, default = False)

parser.add_argument('lang', metavar="lang", nargs='?', help='G2P input language', default = "lv")
args = parser.parse_args()

if args.lang == "lv":
    g2p = G2PConverter(args)
elif args.lang == "lt":
    g2p = G2PConverterLT(args)
else:
    raise NotImplementedError("language not implemented: %s" % args.lang)


if args.dict:
    g2p.load_phondict(args.dict)

if args.excpt:
    g2p.loadExceptions(args.excpt)

filein = sys.stdin
rx_ws = re.compile(r'\s+')

for line in filein:
    try:
        line = line.decode('utf-8').rstrip()
        transcript = line
        tokens = rx_ws.split(transcript)
        if not tokens:
            continue
        graphemes = list()
        if not args.one:
            for t in tokens:
                if t and t[0] == '<' and args.fillers:
                    t = t[1:-1]
                if args.phone:
                    graphemes += g2p.phoneme_to_grapheme(t)
                else:
                    if args.abbr:
                        if g2p.isAbbr(t):
                            graphemes += g2p.convertAbbr(t)
                        else:
                            graphemes += g2p.convert(t)
                    else:
                        graphemes += g2p.convert(t)
            if args.phone:
                graphemeStr = ''.join(graphemes)
            else:
                graphemeStr = ' '.join(graphemes)
            if args.sphinx:
                print ("%s %s" % (transcript, graphemeStr)).encode('utf-8')
            else:
                if args.phone:
                    print graphemeStr
                else:
                    print graphemeStr.encode('utf-8')                    
        else:
            for t in tokens[:1]:
                if t and t[0] == '<' and args.fillers:
                    t = t[1:-1]
                if args.excpt:
                    if g2p.isInExcpList(t):
                        pron = g2p.convertExcpFromList(t).split(" ")
                        for p in pron:
                            print ("%s %s" % (t," ".join(g2p.convert(p)))).encode('utf-8')
                        if '+' in t:
                            if g2p.isAbbr(t):
                                out = " ".join(g2p.convertAbbr(t))
                                if out:
                                    print ("%s %s" % (t,out)).encode('utf-8')
                            else:
                                out = " ".join(g2p.convert(t))
                                if out:
                                    print ("%s %s" % (t,out)).encode('utf-8') 
                    else:
                        #ja tokena nav sarakstā
                        if g2p.isAbbr(t):
                            out = " ".join(g2p.convertAbbr(t))
                            if out:
                                print ("%s %s" % (t,out)).encode('utf-8')
                        else:
                            #tokens nav saīsinājums
                            out = " ".join(g2p.convert(t))
                            if out:
                                print ("%s %s" % (t,out)).encode('utf-8')
                else:
                    #ja nav dota opcija izmantot exceptions
                    print ("%s %s" % (t," ".join(g2p.convert(t)))).encode('utf-8')
    except:
        pass
