#!/bin/bash

datadir=$1
size=$2
outdir=$3

. ./cmd.sh

# copy files from datadir to outdir
utils/copy_data_dir.sh $datadir $outdir

# remove non-speech utterances
grep -v "^.\+ \[.\+\]$" < $datadir/text > $outdir/text
utils/fix_data_dir.sh $outdir

# sort by duration, shortest first
# cut utt2dur after summation
sort -k2n $datadir/utt2dur | python3 -c "
import sys
words = sys.stdin.readlines()
c = 0.0
for line in words:
  word=line.strip().split()[0]
  dur=float(line.strip().split()[1])
  c += dur
  if c > $size * 60 * 60:
      continue
  print (line.strip())
" > $outdir/utt2dur


utils/fix_data_dir.sh $outdir
