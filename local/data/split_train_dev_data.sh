#!/bin/bash

datadir=$1
traindir=$2
devdir=$3

. ./cmd.sh

# copy files from datadir to outdir
utils/copy_data_dir.sh $datadir $traindir
utils/copy_data_dir.sh $datadir $devdir

data_size=`wc -l $datadir/text | cut -f1 -d" "`
((dev_size=data_size/10)) # 10%


shuf $datadir/text > tmp
head -n $dev_size tmp > $devdir/text
tail -n +$dev_size tmp > $traindir/text
rm tmp

utils/fix_data_dir.sh $traindir
utils/fix_data_dir.sh $devdir
