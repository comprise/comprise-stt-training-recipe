#!/bin/bash

njobs=4
lang=$1
stage=0

. ./cmd.sh


echo ============================================================================
echo "                       Data Directory Preparation                         "
echo ============================================================================
# /data/kaldi-data-all, /data/kaldi-data-transcribed, /data/kaldi-data-untranscribed

# merge additional corpora
if [ -d /data/add/speech ]; then
    cp -rl /data/add/speech /data/
fi
python3.7 /api-server/utils/data_prep.py

echo ============================================================================
echo "                           Feature Extraction                             "
echo ============================================================================
# extract features from all directories
for x in /data/kaldi-data-transcribed /data/kaldi-data-untranscribed; do

    utils/utt2spk_to_spk2utt.pl < ${x}/utt2spk > ${x}/spk2utt

    # base features
    steps/make_mfcc.sh --cmd "$train_cmd" --nj $njobs \
     	 	       --mfcc-config conf/mfcc.conf ${x}
    steps/compute_cmvn_stats.sh ${x}

    if [ -f $x/text ]; then
       # remove non-printable characters
       LC_ALL="C.UTF-8" sed -i "s;[^[:print:][:space:]];;g" $x/text
    fi

done

echo ============================================================================
echo "                           Dev/Train split                                "
echo ============================================================================
local/data/split_train_dev_data.sh /data/kaldi-data-transcribed data/train data/dev

echo ============================================================================
echo "                    Lexicon & Language Preparation                        "
echo ============================================================================

if [ $lang == "lt"  ]; then
    local/prepare_dict_lt.sh
else
    local/prepare_dict_espeak.sh --lang $lang
fi
LC_ALL=C utils/prepare_lang.sh data/dict "<unk>" data/lang_tmp data/lang || exit 1;
