#!/bin/bash

# Copyright 2016 Tilde

. path.sh

locdata=/data/kaldi-data-transcribed
lang=lt
addunk=true
addgarb=false

. utils/parse_options.sh

locdict=data/dict

echo "=== Preparing the dictionary ..."

mkdir -p $locdict
echo "=== Getting vocabulary from $locdata/text ..."
cut -f2- -d " " $locdata/text | local/dict.py | cut -f1 > $locdata/vocab-full.txt # save temporary file for debug purposes

grep -E -v "^\[.*\]$" $locdata/vocab-full.txt |\
   local/g2p/g2p_lv.py -s -e local/g2p/exceptions-lt -o $lang |\
   sort -u  > $locdict/lexicon.txt

grep -E "^\[.*\]$" $locdata/vocab-full.txt | sed "s;\(\[.*\]\);\1	SL_SIL;g" >> $locdit/lexicon.txt
sort -u -o $locdict/lexicon.txt $locdict/lexicon.txt

if [ $addgarb == true ]; then
echo "--- Adding <GARB> to the lexicon ..."
echo -e "<GARB>\tSL_GARB" >> $locdict/lexicon.txt
fi

if [ $addunk == true ]; then
echo "--- Adding <unk> to the lexicon ..."
echo -e "<unk>\tSL_GARB" >> $locdict/lexicon.txt
fi

echo "--- Prepare phone lists ..."
grep "SL_" $locdict/lexicon.txt | \
  awk '{for(n=2;n<=NF;n++) { p[$n]=1; }} END{for(x in p) {print x}}' > $locdict/silence_phones.txt
echo SL_SIL >> $locdict/silence_phones.txt
sort -u -o $locdict/silence_phones.txt $locdict/silence_phones.txt

echo SL_SIL > $locdict/optional_silence.txt
grep -v -w SIL $locdict/lexicon.txt | grep -v "SL_" |\
  awk '{for(n=2;n<=NF;n++) { p[$n]=1; }} END{for(x in p) {print x}}' |\
  sort > $locdict/nonsilence_phones.txt

# Some downstream scripts expect this file exists, even if empty
touch $locdict/extra_questions.txt

echo "*** Dictionary preparation finished!"
